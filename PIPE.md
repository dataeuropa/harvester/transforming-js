# Pipe Segment Configuration

The transformer can be configured through the config object as part of the segment body of the pipe.
It allows the configuration of either embedding the script directly in the config object or passing a reference to a git repository:

## _mandatory_

* `scriptType`

    * `embedded` Field `script` contains the scripts directly.
    * `localFile` Field `path` points to a local file inside the `scripts` folder containing the script.
    * `repository` Object `repository` contains information for a script stored in a git repository

Example:

```json
{
  "scriptType": "repository",
  "repository": {
    "uri": "",
    "branch": "",
    "username": "",
    "token": "",
    "script": ""
  }
}
```
