package io.piveau.transforming;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.junit5.VertxExtension;
import io.vertx.junit5.VertxTestContext;
import org.graalvm.polyglot.Context;
import org.graalvm.polyglot.Value;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;

@DisplayName("Testing script engines")
@ExtendWith(VertxExtension.class)
class ScriptTest {

    private final Logger log = LoggerFactory.getLogger(getClass());

    @Test
    void testGraalVMContext(Vertx vertx, VertxTestContext testContext) {
        vertx.fileSystem().readFile("script.js").map(Buffer::toString)
                .onSuccess(script -> {
                    try (Context ctx = Context.newBuilder("js").build()) {
                        ctx.eval("js", script);
                        ctx.eval("js", "var params = {};");
                        Value value = ctx.eval("js", "(function(obj) { return JSON.stringify(transforming(obj)) })");
                        Value result = value.execute("{}");
                        log.debug("Value: {}", result.asString());
                        testContext.completeNow();
                    } catch (Exception e) {
                        testContext.failNow(e);
                    }
                })
                .onFailure(testContext::failNow);
    }

    @Test
    void testScriptEngine(Vertx vertx, VertxTestContext testContext) {
        vertx.fileSystem().readFile("script.js").map(Buffer::toString)
                .onSuccess(script -> {
                    ScriptEngine engine = new ScriptEngineManager().getEngineByName("graal.js");
                    try {
                        engine.eval(script);
                        engine.eval("function executeTransformation(obj) { return JSON.stringify(transforming(JSON.parse(obj))) }");
                        engine.eval("var params = {};");

                        Invocable jsInvoke = (Invocable) engine;
                        Object output = jsInvoke.invokeFunction("executeTransformation", "{}");
                        log.debug("Value: {}", output.toString());

                        testContext.completeNow();
                    } catch (Exception e) {
                        testContext.failNow(e);
                    }
                })
                .onFailure(testContext::failNow);
    }
}
