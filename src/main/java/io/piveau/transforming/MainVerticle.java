package io.piveau.transforming;

import io.piveau.pipe.connector.PipeConnector;
import io.piveau.transforming.js.JsTransformingVerticle;
import io.piveau.transforming.repositories.RepositoryVerticle;
import io.vertx.core.*;

import java.util.Arrays;

public class MainVerticle extends AbstractVerticle {

    @Override
    public void start(Promise<Void> startPromise) {
        vertx.deployVerticle(RepositoryVerticle.class, new DeploymentOptions())
                .compose(id -> vertx.deployVerticle(JsTransformingVerticle.class, new DeploymentOptions().setWorker(true)))
                .compose(id -> PipeConnector.create(vertx))
                .onSuccess(connector -> {
                    connector.publishTo(JsTransformingVerticle.ADDRESS, false);
                    startPromise.complete();
                })
                .onFailure(startPromise::fail);
    }

    // Allows to start from IDEs
    public static void main(String[] args) {
        String[] params = Arrays.copyOf(args, args.length + 1);
        params[params.length - 1] = MainVerticle.class.getName();
        Launcher.executeCommand("run", params);
    }

}
