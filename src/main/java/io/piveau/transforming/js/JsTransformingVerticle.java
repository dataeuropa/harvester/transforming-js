package io.piveau.transforming.js;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.json.JsonReadFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.piveau.pipe.PipeContext;
import io.piveau.transforming.repositories.RepositoryVerticle;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Promise;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import org.ehcache.Cache;
import org.ehcache.CacheManager;
import org.ehcache.config.builders.CacheConfigurationBuilder;
import org.ehcache.config.builders.CacheManagerBuilder;
import org.ehcache.config.builders.ExpiryPolicyBuilder;
import org.ehcache.config.builders.ResourcePoolsBuilder;
import org.ehcache.config.units.EntryUnit;

import javax.script.*;
import java.io.*;
import java.time.Duration;
import java.util.UUID;

public class JsTransformingVerticle extends AbstractVerticle {

    public static final String ADDRESS = "io.piveau.pipe.transformation.js.queue";

    private Cache<String, Future> cache;

    private ObjectMapper mapper;

    private final String instanceId = UUID.randomUUID().toString();

    @Override
    public void start(Promise<Void> startPromise) {
        vertx.eventBus().consumer(ADDRESS, this::handlePipe);

        CacheManager cacheManager = CacheManagerBuilder.newCacheManagerBuilder()
                .withCache("transformer-" + instanceId, CacheConfigurationBuilder.newCacheConfigurationBuilder(String.class, Future.class,
                                ResourcePoolsBuilder.newResourcePoolsBuilder().heap(20, EntryUnit.ENTRIES))
                        .withExpiry(ExpiryPolicyBuilder.timeToIdleExpiration(Duration.ofHours(2))))
                .build(true);
        cache = cacheManager.getCache("transformer-" + instanceId, String.class, Future.class);

        mapper = new ObjectMapper();
        mapper.configure(JsonReadFeature.ALLOW_UNESCAPED_CONTROL_CHARS.mappedFeature(), true);
        mapper.configure(JsonReadFeature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER.mappedFeature(), true);
        mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);

        startPromise.complete();
    }

    private void handlePipe(Message<PipeContext> message) {
        PipeContext pipeContext = message.body();
        pipeContext.log().trace("Incoming pipe");
        JsonObject config = pipeContext.getConfig();

        JsonObject dataInfo = pipeContext.getDataInfo();

        if (!dataInfo.containsKey("content") || dataInfo.getString("content").equals("metadata")) {

            String runId = pipeContext.getPipe().getHeader().getRunId();

            getOrCreateScriptEngine(runId, config)
                    .onSuccess(engine -> {

                        Invocable jsInvoke = (Invocable) engine;

                        try {
                            JsonNode input = mapper.readTree(pipeContext.getStringData());
                            if (pipeContext.log().isDebugEnabled()) {
                                pipeContext.log().debug("Transformation input:\n{}", input.toPrettyString());
                            }

                            Object output = jsInvoke.invokeFunction("executeTransformation", input.toString());

                            String out = output.toString();
                            if (pipeContext.log().isDebugEnabled()) {
                                pipeContext.log().debug("Transformation result:\n{}", out);
                            }

                            String outputFormat = config.getString("outputFormat", "application/ld+json");
                            pipeContext.setResult(out, outputFormat, dataInfo).forward();
                            pipeContext.log().info("Transformation finished: {}", dataInfo);

                        } catch (IOException | NoSuchMethodException | ScriptException e) {
                            pipeContext.setFailure(e);
                        }
                    })
                    .onFailure(pipeContext::setFailure);
        } else {
            pipeContext.log().trace("Passing pipe");
            pipeContext.pass();
        }
    }

    private Future<ScriptEngine> getOrCreateScriptEngine(String runId, JsonObject config) {
        boolean useCache = config.getBoolean("single", true);
        if (useCache && cache.containsKey(runId)) {
            return (Future<ScriptEngine>)cache.get(runId);
        } else {
            return Future.future(promise -> {
                if (useCache) {
                    cache.put(runId, promise.future());
                }
                getScript(config)
                        .onSuccess(script -> {
                            try {

                                ScriptEngine engine = new ScriptEngineManager().getEngineByName("graal.js");
                                engine.eval(script);
                                engine.eval("function executeTransformation(obj) { return JSON.stringify(transforming(JSON.parse(obj))) }");

                                if (config.containsKey("params")) {
                                    engine.eval("var params = " + config.getJsonObject("params").encode() + ";");
                                } else {
                                    engine.eval("var params = {};");
                                }

                                promise.complete(engine);
                            } catch (Throwable t) {
                                promise.fail(t);
                            }
                        })
                        .onFailure(promise::fail);
            });
        }
    }

    public Future<String> getScript(JsonObject config) {
        return Future.future(promise -> {
            switch (config.getString("scriptType", "")) {
                case "repository" -> {
                    JsonObject repository = config.getJsonObject("repository");
                    vertx.eventBus().<String>request(RepositoryVerticle.ADDRESS, repository)
                            .onSuccess(reply -> promise.complete(reply.body()))
                            .onFailure(promise::fail);
                }
                case "localFile" -> {
                    String path = "scripts/" + config.getString("path", "default.js");
                    vertx.fileSystem().exists(path)
                            .compose(exists -> {
                                if (Boolean.TRUE.equals(exists)) {
                                    return vertx.fileSystem().readFile(path).map(Buffer::toString);
                                } else {
                                    return Future.failedFuture("Script " + path + " not found");
                                }
                            }).onComplete(promise);
                }
                case "embedded" -> {
                    String script = config.getString("script", "");
                    if (!script.isBlank()) {
                        promise.complete(script);
                    } else {
                        promise.fail("Invalid embedded script");
                    }
                }
                default -> promise.fail("Unknown script type");
            }

        });
    }

}
